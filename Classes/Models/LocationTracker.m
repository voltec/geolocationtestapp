//
//  LocationTracker.m
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import "LocationTracker.h"

const double MinimumMetersPerSecond = 9/3.6;//9km/h

@interface LocationTracker ()<CLLocationManagerDelegate> {
    CLLocation *prevLocation;
}

@property (strong, readwrite, nonatomic) CLLocation * _Nullable lastCurrentLocation;

@property (strong, nonatomic) CLLocationManager *locationManager;


@property (nonatomic) BOOL trackingStarted;

@end

@implementation LocationTracker

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = MinimumMetersPerSecond;
        self.locationManager.delegate = self;
        if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
            [self.locationManager setAllowsBackgroundLocationUpdates:YES];
        }
        self.locationManager.pausesLocationUpdatesAutomatically = YES;
        [self.locationManager requestAlwaysAuthorization];
        
    }
    return self;
}

#pragma MARK - Public

+ (BOOL)isAutorized {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    return (status == kCLAuthorizationStatusAuthorizedAlways
            || status == kCLAuthorizationStatusAuthorizedWhenInUse);
}

- (void)startMonitoring {
    [self.locationManager startUpdatingLocation];
}

- (void)stopMonitoring {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - Private

- (void)setTrackingStarted:(BOOL)trackingStarted {
    if (_trackingStarted != trackingStarted) {
        _trackingStarted = trackingStarted;
        
        if (_trackingStarted) {
            NSLog(@"didStartInLocation");
            [self.delegate locationTracker:self didStartInLocation:self.lastCurrentLocation];
        }
        else {
            NSLog(@"didStopInLocation");
            [self.delegate locationTracker:self didStopInLocation:self.lastCurrentLocation];
        }
    }
}

#pragma MARK - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *loc = locations.lastObject;
    prevLocation = self.lastCurrentLocation;
    
    self.lastCurrentLocation = loc;
    
    if (self.lastCurrentLocation) {
        if (self.lastCurrentLocation.speed > MinimumMetersPerSecond) {
            if (prevLocation == nil || prevLocation.speed < MinimumMetersPerSecond) {
                self.trackingStarted = YES;
            }
            else {
                [self.delegate locationTracker:self didUpdateLocation:self.lastCurrentLocation];
            }
        }
        else {
            if (prevLocation && prevLocation.speed > MinimumMetersPerSecond) {
                self.trackingStarted = NO;
            }
        }
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    prevLocation = nil;    
    self.trackingStarted = NO;
    self.lastCurrentLocation = nil;
}

@end
