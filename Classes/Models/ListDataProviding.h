//
//  ListDataProviding.h
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ListDataProviderDelegate;

@protocol ListDataProviding <NSObject>

@property (weak, nonatomic) id delegate;

@optional

- (void)updateData;

@required
- (NSArray *)allObjects;
- (NSInteger)numberOfObjects;
- (id)objectAtIndex:(NSInteger)index;
- (NSInteger)indexOfObject:(id)object;

@end

#pragma mark - ListDataProviderChangeType

typedef NS_ENUM(NSInteger, ListDataProviderChangeType) {
    ListDataProviderChangeInsert,
    ListDataProviderChangeDelete,
    ListDataProviderChangeMove,
    ListDataProviderChangeUpdate,
};

@protocol ListDataProviderDelegate <NSObject>

@optional
- (void)listDataProviderReloadData:(id<ListDataProviding>)listDataProvider;
- (void)listDataProviderWillBeginChanges:(id<ListDataProviding>)listDataProvider;
- (void)listDataProvider:(id<ListDataProviding>)listDataProvider didChangeObjectAtIndex:(NSInteger)index
                                        forChangeType:(ListDataProviderChangeType)type newIndex:(NSInteger)newIndex;
- (void)listDataProviderDidEndChanges:(id<ListDataProviding>)listDataProvider;


@end
