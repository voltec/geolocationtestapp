//
//  LocationTracker.h
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

@class LocationTracker;

@protocol LocationTrackerDelegate <NSObject>

- (void)locationTracker:(LocationTracker *_Nonnull)locationTracker didUpdateLocation:(CLLocation * _Nonnull)location;

- (void)locationTracker:(LocationTracker *_Nonnull)locationTracker didStartInLocation:(CLLocation * _Nonnull)location;

- (void)locationTracker:(LocationTracker *_Nonnull)locationTracker didStopInLocation:(CLLocation * _Nonnull)location;

@end

@interface LocationTracker : NSObject

@property (weak, nonatomic, nullable) id<LocationTrackerDelegate> delegate;

@property (strong, readonly, nonatomic) CLLocation * _Nullable lastCurrentLocation;

+ (BOOL)isAutorized;
- (void)startMonitoring;
- (void)stopMonitoring;

@end
