//
//  MapAnnotation.m
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import "MapAnnotation.h"

@interface MapAnnotation()

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy, nullable) NSString *title;
@property (nonatomic, copy, nullable) NSString *subtitle;

@end

@implementation MapAnnotation

- (instancetype)initWithLocation:(CLLocation *)location
{
    self = [super init];
    if (self) {
        NSString *unit = NSLocalizedString(@"km/h", @"Speed unit");
        self.title = [NSString stringWithFormat:@"%0.0f %@", location.speed*MeterPerSekToKmPerHour, unit];
        self.coordinate = location.coordinate;
    }
    return self;
}

@end
