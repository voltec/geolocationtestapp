//
//  MapDataProvider.h
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "LocationTracker.h"

#import "MapDataProviding.h"

@interface MapDataProvider : NSObject<MapDataProviding>

@property (strong, nonatomic) LocationTracker *tracker;

@property (weak, nonatomic) id delegate;
@property (readonly, nonatomic) NSArray<CLLocation *> *locations;

@end
