//
//  main.m
//  TemplateObjc
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
