//
//  MapDataProviding.h
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ListDataProviding.h"

@protocol MapDataProviderDelegate;

@protocol MapDataProviding <ListDataProviding>

@end

@protocol MapDataProviderDelegate <ListDataProviderDelegate>

@optional

- (void)mapDataProvider:(id<MapDataProviding>)listDataProvider didUpdateSpeed:(float)speed;

@end
