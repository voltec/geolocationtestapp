//
//  MapDataProvider.m
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import "MapDataProvider.h"

@interface MapDataProvider ()<LocationTrackerDelegate>

@property (strong, nonatomic) NSMutableArray<CLLocation *> *privateLocations;
@property (copy, nonatomic) NSArray<CLLocation *> *locations;

@end

@implementation MapDataProvider

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.privateLocations = [NSMutableArray new];
        
        self.tracker = [[LocationTracker alloc] init];
        self.tracker.delegate = self;
        [self.tracker startMonitoring];
    }
    return self;
}

- (void)dealloc
{
    [self.tracker stopMonitoring];
}

- (NSArray *)allObjects {
    return self.locations;
}

- (NSInteger)numberOfObjects {
    return self.locations.count;
}

- (id)objectAtIndex:(NSInteger)index {
    if (index > 0 && index < self.locations.count) {
        return self.locations[index];
    }
    return nil;
}

- (NSInteger)indexOfObject:(id)object {
    return [self.locations indexOfObject:object];
}


#pragma mark - LocationTrackerDelegate

- (void)locationTracker:(LocationTracker *_Nonnull)locationTracker didUpdateLocation:(CLLocation * _Nonnull)location {
    [self.privateLocations addObject:location];
    if ([self.delegate respondsToSelector:@selector(mapDataProvider:didUpdateSpeed:)])
        [self.delegate mapDataProvider:self didUpdateSpeed:location.speed*MeterPerSekToKmPerHour];
}

- (void)locationTracker:(LocationTracker *_Nonnull)locationTracker didStartInLocation:(CLLocation * _Nonnull)location {
    self.locations = nil;
    [self.privateLocations removeAllObjects];
    [self.privateLocations addObject:location];
    if ([self.delegate respondsToSelector:@selector(listDataProviderReloadData:)])
        [self.delegate listDataProviderReloadData:self];
}

- (void)locationTracker:(LocationTracker *_Nonnull)locationTracker didStopInLocation:(CLLocation * _Nonnull)location {
    if (location) {
        [self.privateLocations addObject:location];
    }
    self.locations = self.privateLocations;
    if ([self.delegate respondsToSelector:@selector(listDataProviderReloadData:)])
        [self.delegate listDataProviderReloadData:self];
}

@end
