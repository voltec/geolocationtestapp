//
//  MapViewController.m
//  GeolocationTestApp
//
//  Created by Mukhail Mukminov on 24.06.16.
//  Copyright © 2016 Mukhail Mukminov. All rights reserved.
//

#import "MapViewController.h"
#import "MapDataProvider.h"

#import "MapAnnotation.h"

#import "NSArray+Map.h"

#import <MapKit/MapKit.h>

@interface MapViewController ()<ListDataProviderDelegate> {
    
    id<MapDataProviding> dataProvider;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (copy, nonatomic) NSArray<MapAnnotation *> *annotations;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataProvider = [MapDataProvider new];
    dataProvider.delegate = self;
    self.speedLabel.superview.hidden = YES;
    
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - IBAction

- (IBAction)userLocationClick:(id)sender {
    if (self.mapView.userLocation) {        
        [self.mapView setCenterCoordinate:self.mapView.userLocation.location.coordinate animated:YES];
        //without zoom
    }
}

#pragma mark - Map points

- (void)resetAnnotations {
    [self.mapView removeAnnotations:self.annotations];
    self.annotations = nil;
}

- (void)retrieveMapPoints {
    [self resetAnnotations];
    NSArray<CLLocation *> *locations = [dataProvider allObjects];
    if (locations.count > 0) {
        NSMutableArray<MapAnnotation *> *annotations = [NSMutableArray new];
        for (CLLocation *location in locations) {
            MapAnnotation *annotation = [[MapAnnotation alloc] initWithLocation:location];
            [annotations addObject:annotation];
        }
        self.annotations = annotations;
        [self.mapView addAnnotations:self.annotations];
    }
}

#pragma mark - ListDataProviderDelegate

- (void)listDataProviderReloadData:(id<MapDataProviding>)listDataProvider {
    self.speedLabel.superview.hidden = YES;
    [self resetAnnotations];
    [self retrieveMapPoints];
}

- (void)mapDataProvider:(id<MapDataProviding>)listDataProvider didUpdateSpeed:(float)speed {
    self.speedLabel.superview.hidden = NO;
    NSString *unit = NSLocalizedString(@"km/h", @"Speed unit");
    self.speedLabel.text = [NSString stringWithFormat:@"%0.0f %@", speed, unit];
}

@end
